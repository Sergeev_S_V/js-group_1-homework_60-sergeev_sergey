import React, { Component } from 'react';
import './App.css';
import Messages from "../components/Messages";
import AddForm from "../components/AddForm/AddForm";


class App extends Component {

  url = 'http://146.185.154.90:8000/messages';

  state = {
    messages: [],
    myMessage: '',
  };
  interval = null;

  changeName = (event) => {
    this.setState({myMessage: event.target.value});
  };

  addMessage = () => {
    let formData = new URLSearchParams();
    formData.append('author', 'Gendalf');
    formData.append('message', this.state.myMessage);

    fetch(this.url, {
      method: 'post',
      body: formData,
    }).then(resp => resp.json());
  };

  getMessages = () => {
    fetch(this.url)
      .then(massages => massages.json())
      .then(massages => {
        this.setState({messages: massages});
      });
  };

  componentDidMount() {
    let lastDate = '';
    fetch(this.url)
      .then(massages => massages.json())
      .then(massages => {
        lastDate = massages[massages.length - 1].datetime;
      });
    this.interval = setInterval(() => {
      fetch('http://146.185.154.90:8000/messages?datetime=' + lastDate)
        .then(massages => {
          return massages.json();
        })
        .then(resp => {
          let copyMassages = [...this.state.messages];
          if (resp.length > 0) {
            lastDate = resp[resp.length - 1].datetime;
            resp.forEach(massages => copyMassages.push(massages));
            this.setState({messages: copyMassages});
          }
        })
    }, 2000);
    this.getMessages();
  }

  clear = () => {
    clearInterval(this.interval);
  };

  componentWillUnmount() {
    this.clear();
  }

  render() {
    return (
      <div className="App">
        <AddForm myMessage={this.state.myMessage}
                 change={(event) => this.changeName(event)}
                 add={this.addMessage}/>
        <Messages messages={this.state.messages}/>
      </div>
    );
  }
}

export default App;
