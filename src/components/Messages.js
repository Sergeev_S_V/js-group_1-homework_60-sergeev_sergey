import React, {Component} from 'react';
import Message from "./Message";
import './Messages.css';

class Messages extends Component {
  render () {
    return (
      <div id='Messages'>
        {this.props.messages.map(messages => {
          return (
            <Message message={messages.message}
                     author={messages.author}
                     datetime={messages.datetime}
                     key={messages.datetime}/>
          );
        })}
      </div>
    )
  }
}

export default Messages;