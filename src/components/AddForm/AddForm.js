import React, {Component} from 'react';
import './AddForm.css';

class AddForm extends Component {
  render () {
    return (
      <div className='AddForm'>
        <input type="text" placeholder='Enter your massage'
               className='enterTxt'
               onChange={this.props.change}
        />
        <button className='AddForm-Btn' onClick={this.props.add}>ADD</button>
      </div>
    )
  }
}

export default AddForm;