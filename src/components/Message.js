import React, {Component} from 'react';
import './Message.css';

class Message extends Component {
  render() {
    return (
      <div className='Message'>
        <p>Author: {this.props.author}</p>
        <p>Say: {this.props.message}</p>
        <p className='date' style={{float: 'left'}}>Date: {this.props.datetime}</p>
      </div>
    );
  }
}

export default Message;